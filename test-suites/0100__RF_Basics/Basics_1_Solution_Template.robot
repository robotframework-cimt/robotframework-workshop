*** Settings ***
Documentation  Basic examples with Robot Framework data types, libraries, error handling and logging.
Library  BuiltIn  # not necessary to declare
Library  String


*** Variables ***
&{mountain_elevation}=  Brocken=1141  Zugspitze=2962
@{list}  1  2  3  4  5  6  7  42
${seven_number}  ${7}
${seven_string}  7


*** Test Cases ***
Example: Numbers
  Should Not Be Equal  ${seven_number}  7
  Should Be Equal  ${seven_string}  7
  Should Not Be Equal  ${seven_string}  ${seven_number}
  Should Be Equal As Integers  ${seven_number}  7
  ${seven_number}=  Convert To String  ${seven_number}
  Should Be Equal  ${seven_number}  7

Example: Dictionary - Log Mountain Elevation
  Log Many  &{mountain_elevation}
  Log  ${mountain_elevation}[Brocken]
  Log To Console  ${mountain_elevation}[Zugspitze]

Log variable data types
  Log To Console    In this test case you should log the data types of the variables in the variables section.

Working with lists
  Log To Console    In this test case you should compare two list variables.

Working with dictionaries
  Log To Console    In this test case you should do some dictionary operations.