*** Settings ***
Documentation  Example for generating and using a (java) remote library.
Library  Remote  http://127.0.0.1:8270/  WITH NAME  JavaRemote


*** Test Cases ***
Example
  ${return_value}=  JavaRemote.Print Message  Tadaaa!
  Log  ${return_value}

Exercise 1a
  Log To Console    In this test case you should join two string using a custom Java keyword.

Exercise 1b
  Log To Console    In this test case you should count the number of words in a file using a custom Java keyword.