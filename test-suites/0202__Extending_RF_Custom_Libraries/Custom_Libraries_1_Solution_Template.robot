*** Settings ***
Documentation  Example of how to extend Robot Framework by your own python keyword library
...            and generating documentation for libraries. 
Library  CustomPython.py
Library  String


*** Test Cases ***
Example
  ${answer}=  Return Answer    arg
  Log  ${answer}

Exercise 1a
  Log To Console    In this test case you should join two string using a custom Python keyword.

Exercise 1b
  Log To Console    In this test case you should count the number of words in a file using a custom Python keyword.