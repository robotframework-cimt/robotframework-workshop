# Robot Framework Workshop

## Exercises

### Basics 1
**Prerequisites:**
* Robot Framework installed
  
      pip install robotframework


**In this exercise you will learn:**
* basic libraries **[BuiltIn](https://robotframework.org/robotframework/latest/libraries/BuiltIn.html)**   and **[String](https://robotframework.org/robotframework/latest/libraries/String.html)**
* Robot Framework data types (string, number, boolean)
* using lists and dictionaries (**[Collections](https://robotframework.org/robotframework/latest/libraries/Collections.html)** library)
* handling errors (**[OperatingSystem](https://robotframework.org/robotframework/latest/libraries/OperatingSystem.html)** library)
* logging

**Exercise:**
1. Open test-suites/0100__RF_Basics/Basics_1_Examples.robot
2. Inspect and run examples
   1. Inspect defined variables and different data types
   2. Run test suite
   
            robot test-suites/0100__RF_Basics/Basics_1_Examples.robot
   
3. In the *Basics_1_Solution_Template* file log the data types of the defined variables using the keywords/functions
   1. *Log To Console* (BuiltIn library),
   2. *Evaluate* (BuiltIn library) and 
   3. type(\$variable) or type(\$variable).\_\_name\_\_
   
4. Working with lists
   1. Create a second list @{list_2} using the keyword *Create List* with a different value at position 6
   2. Write a test case that compares the lists @{list} and @{list_2} using the Collections library

      *hint: you can prevent the test suite from failing using the keyword Run Keyword And Return Status ahead of a keyword that is expected to fail.*

   3. Start the test suite with parameter -d logs
   
            robot -d logs test-suites/0100__RF_Basics/Basics_1_Examples.robot

    4. Inspect logs/report.html
   
5. Working with dictionaries
   1. Verify that the mountain_elevation dictionary does not contain 'Watzmann'
   2. Create a second dictionary ${city_population} with the entries Berlin(~3.8 million) and Hamburg(~1.9)
   3. Create a list ${german_facts} containing the two dictionaries and log the population of hamburg using the created list



### Basics 2
**Prerequisites:**
* --

**In this exercise you will learn:**
* interacting with files (**[OperatingSystem](https://robotframework.org/robotframework/latest/libraries/OperatingSystem.html)** library)
* handling content of files (**[String](https://robotframework.org/robotframework/latest/libraries/String.html)** library)
* using a for loop

**Exercise:**
1. Open test-suites/0100__RF_Basics/Basics_2_Examples.robot
2. Inspect and run examples
3. Write a test case to check whether file exists and the content is correct (use **OperatingSystem** library)
4. Write a test case that splits the file content into its single fields/values and create a two-dimensional list. Log the list (*Log  ${2darray}*). The result should look like
   
        ``` [['Mueller', '42', 'Hamburg'], ['Meier', '67', 'Muenchen'], ['Wuensch', '38', 'Koeln']] ```
   
   *hint: use the String library and for loop*
